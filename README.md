prosilverRed
============

A red version of the prosilver phpBB theme.

If incompatible with the current phpBB release, use git to merge changes from the most recent prosilver version.

Packaging
---------

Don't include the nopackage/ and README.md files in the style.zip. These files are for developer reference.
